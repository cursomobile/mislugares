package com.example.mislugares;

import java.util.ArrayList;
import java.util.List;

public class Lugares {
    protected static List<Lugar> vectorLugares = ejemploLugares();

    public Lugares(){

        vectorLugares = ejemploLugares();
    }


    static Lugar elemento (int id){

        return vectorLugares.get(id);
    }

    static void anyade (Lugar lugar){

        vectorLugares.add(lugar);
    }

    static int nuevo(){
        Lugar lugar = new Lugar();
        vectorLugares.add(lugar);
        return vectorLugares.size()-1;
    }

    static void borrar (int id){
        vectorLugares.remove(id);
    }

    public static int size() {
        return vectorLugares.size();
    }


    /* Estructura para poder inicializar los datros de la aplicacion*/

    public static ArrayList<Lugar> ejemploLugares() {
        ArrayList<Lugar> lugares = new ArrayList<Lugar>();

        lugares.add(new Lugar("Lima", TipoLugar.EDUCACION, "Jiron Parinacochas 236",  "http://www.epsg.upv.es","962849300", "Uno de los mejores lugares para formarse.", "23/08/2015",3));
        lugares.add(new Lugar("Arequipa",TipoLugar.BAR, "Avenida Goyeneche 376 Cercado Arequipa", "636472405", "http://www.arequipa.pe", "No te pierdas el arroz en calabaza.", "23/08/2015",3));
        lugares.add(new Lugar("Cusco",TipoLugar.EDUCACION, "Avenida del Inca 984 cercado de Cuzco", "98754216","http://androidcurso.com", "Amplia tus conocimientos sobre Android.", "23/08/2015",5));
        lugares.add(new Lugar("Trujillo", TipoLugar.NATURALEZA,"Vía Verde del río ",  "http://sosegaos.blogspot.com.es/","9856324",  "Espectacular ruta para bici o andar", "23/08/2015",4));
        lugares.add(new Lugar("Iquitos",TipoLugar.COMPRAS, "Avda. Amazonas 387 Nauta",   "http://www.lavital.es/",  "962881070","El típico centro comercial", "23/08/2015",2));

        return lugares;
    }
}

