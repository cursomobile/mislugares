import com.example.mislugares.GeoPunto;
import com.example.mislugares.TipoLugar;

public class Lugar {
    private String nombre;
    private TipoLugar tipo;
    private GeoPunto posicion;
    private String foto;
    private String direccion;
    private String url;
    private String telefono;
    private String comentario;
    private String fecha;
    private float valoracion;

    public Lugar(){

    }


    public Lugar(String nombre, TipoLugar tipo, String direccion, String url, String telefono, String comentario, String fecha, int valoracion) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.direccion = direccion;
        this.url = url;
        this.telefono = telefono;
        this.comentario = comentario;
        this.fecha = fecha;
        this.valoracion = valoracion;

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoLugar getTipo() {
        return tipo;
    }

    public void setTipo(TipoLugar tipo) {
        this.tipo = tipo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public float getValoracion() {
        return valoracion;
    }

    public void setValoracion(float valoracion) {
        this.valoracion = valoracion;
    }

    public GeoPunto getPosicion() {
        return posicion;
    }

    public void setPosicion(GeoPunto posicion) {
        this.posicion = posicion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
